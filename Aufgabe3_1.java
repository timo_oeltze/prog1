package main;

import Prog1Tools.IOTools;

import java.util.*;
import java.util.stream.Stream;

public class ColorCode {

    public static void main(String[] args) {
        char[] charArray = generateRandomArray();

        //System.out.println("[ A: " + analyzeArray(test1, test2)[0] + " | B: " + analyzeArray(test1, test2)[1] + " ]");

    }

    private static void printArray( char[] charArray ) {
        System.out.println("Current Array: " + arrayToString(charArray));
    }

    private static char[] generateRandomArray() {
        char[] charArray = new char[4];

        //befülle charArray mit random Chars
        for (int i = 0; i < charArray.length; i++) {
            charArray[i] = charGenerator();
        }

        return charArray;
    }

    private static char charGenerator() {
        //Erstellen einen Generator
        Random r = new Random();

        //String aus dem chars zufällig ausgewählt werden
        String valueOfChars = "yorgcb";

        /*
         * Übergebe die Länge des Strings an den Generator,
         * der an die Funktion charAt einen random Wert übergibt
         * uns so zufällig ein char aus dem String nimmt
         */
        return valueOfChars.charAt(r.nextInt(valueOfChars.length()));
    }

    private static String arrayToString( char[] charArray ) {
        StringBuilder toString = new StringBuilder();

        //Durchlaufen des Arrays und anhängen jedes gespeicherten Chars an den Stringbuilder
        for (char charAt : charArray) {
            toString.append(charAt).append(".");
        }

        //StringBuilder in ein String umwandeln und zurückgeben
        return toString.toString();
    }

    private static void changeArray( char[] charArray ) {
        int inputIndex;
        char inputChar;

        //auf ersten int Wert warten und dann pürfen, ob dieser zwischen 0 und der länge des Arrays ligt
        do {
            inputIndex = IOTools.readInt("Which Index? ");
        } while ( inputIndex < 0 || charArray.length-1 < inputIndex );


        //wie darüber, nur das mit legitCharInput geprüft wird, dass die Eingabe korrekt ist
        do {
            inputChar = IOTools.readChar("Which char?: ");
        } while ( !legitCharInput(inputChar) );

        charArray[inputIndex] = inputChar;
    }

    private static boolean legitCharInput( char input ) {
        /*
         * Character.toString(input) Character in String umwandeln
         * überprüfen ob der umgewandelte Character in yorgcb vorkommt und somit gültig ist
         * equalsIgnoreCase akzeptiert den String groß und klein geschreiben
         */

        return "yorgcb".contains(Character.toString(input));
    }

    private static boolean exitProgram() {
        String input;
        String accept = "yn";

        do {
            input = IOTools.readString("Done? (y / n): ");
        } while (accept.equals(input));

        return input.equals("y");
    }

    private static int[] analyzeArray( char[] userArray, char[] computerArray ) {
        int[] resultArray = new int[2];
        resultArray[0] = samePosition(computerArray, userArray);
        resultArray[1] = wrongPosition(computerArray, userArray);

        return resultArray;

    }

    private static int samePosition( char[] computer, char[] user ) {
        int count = 0;
        for ( int i = 0; i < user.length; i++ ) {
            if (computer[i] == user[i]) {
                count++;
            }
        }

        return count;
    }

    private static int wrongPosition( char[] computer, char[] user ) {
        List<Character> inBothArrays = new ArrayList<>();
        int count = 0;

        for (char charUser : user) {
            for (char charComputer : computer) {
                if (charComputer == charUser && !inBothArrays.contains(charUser)) {
                    inBothArrays.add(charUser);
                }
            }
        }


        for (char inBoth : inBothArrays) {
            for (int i = 0; i < computer.length; i++) {
                if (inBoth == computer[i] && computer[i] != user[i]) {
                    count++;
                }
            }
        }

        return count;
    }
}